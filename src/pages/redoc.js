import Layout from "@theme/Layout";
import React from "react";
import { RedocStandalone } from "redoc";

const ReDocly = () => {
  return (
    <Layout
      title={`Swagger`}
      description="Description will go into a meta tag in <head />"
    >
      <main>
        <RedocStandalone spec="./api.spec.json" />
      </main>
    </Layout>
  );
};

export default ReDocly;
