import Layout from "@theme/Layout";
import React from "react";
import SwaggerUI from "swagger-ui-react";
import "swagger-ui-react/swagger-ui.css";

const Swagger = () => {
  return (
    <Layout
      title={`Swagger`}
      description="Description will go into a meta tag in <head />"
    >
      <main>
        <SwaggerUI url="/api.spec.json" />
      </main>
    </Layout>
  );
};

export default Swagger;
