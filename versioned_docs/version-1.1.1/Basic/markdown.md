---
title: Markdown
description: Markdown Review
slug: /markdown
---

## Heading

```
# H1
## H2
### H3
```

With ID

```
### My Great Heading {#custom-id}
```

### My Great Heading {#custom-id}

## Bold

```
**bold text**
```

This text contains **bold text** in it.

## Italic

```
*italicized text*
```

This text contains _italicized text_ in it.

## Blockquote

```
> blockquote
```

> blockquote

## Ordered List

1. First item
2. Second item
3. Third item

## Unordered List

- First item
- Second item
- Third item

## Code

```
`code`
```

`code`

## Horizontal Rule

```
---
```

---

## Link

```
[title](https://www.example.com)
```

[link title](https://www.example.com)

## Image

```
![alt text](./img/localeDropdown.png)
```

![alt text](./img/localeDropdown.png)

[**To Great Heading**](#custom-id)
