```mermaid
sequenceDiagram
    participant Client
    participant AuthController
    participant AuthService
    participant UsersRefreshtokenService
    participant UsersService
    Client->>AuthController : POST : refresh-token
    activate AuthController
    AuthController->>AuthService : Send request
    activate AuthService
    AuthService-->>AuthService : Verify refreshToken
    alt token's expired
        AuthService-->>AuthController : Send error & StatusCode 401
    else
        AuthService->>UsersRefreshtokenService : Check usersRefreshtoken
        activate UsersRefreshtokenService
        UsersRefreshtokenService-->>AuthService : Send response
        deactivate UsersRefreshtokenService
        alt usersRefreshtoken isn't found
            AuthService-->>AuthController : Send error & StatusCode 403
        else
            AuthService->>UsersService : Find user
            activate UsersService
            UsersService-->>AuthService : Send response
            deactivate UsersService
            alt user isn't found
                AuthService-->>AuthController : Send error & StatusCode 404
            else
                AuthService->>CommonService : Get expire accessToken
                activate CommonService
                CommonService-->>AuthService : Send response
                deactivate CommonService
                AuthService->>CommonService : Get expire refreshToken
                activate CommonService
                CommonService-->>AuthService : Send response
                deactivate CommonService
                AuthService->>CommonService : Get expire iat
                activate CommonService
                CommonService-->>AuthService : Send response
                deactivate CommonService
                AuthService-->>AuthService : Sign accessToken
                AuthService-->>AuthController : Send response & StatusCode 200
            end
        end
    end
    deactivate AuthService
    AuthController-->>Client : Send response
    deactivate AuthController
```