```mermaid
sequenceDiagram
    participant Client
    participant AuthController
    participant LocalAuthGuard
    participant AuthService
    participant users(DB)
    participant UsersLoginService
    participant userLogin(DB)
    participant CommonService
    participant UsersRefreshtokenService
    Client->>AuthController : POST : login
    activate AuthController
    AuthController->>LocalAuthGuard : Send request
    activate LocalAuthGuard
    LocalAuthGuard->>AuthService : Send Request 
    deactivate LocalAuthGuard
    activate AuthService
    AuthService->>users(DB) : Query user
    activate users(DB)
    users(DB)-->>AuthService : Send response 
    deactivate users(DB)
    alt user isn't found
        AuthService-->>AuthController : Send StatusCode 404
    else
        AuthService-->>AuthService : Check password
        alt password's correct
            AuthService->>UsersLoginService : Send request
            activate UsersLoginService
            UsersLoginService->>userLogin(DB) : Create userLogin
            deactivate UsersLoginService
        else
            AuthService->>UsersLoginService : Send request
            activate UsersLoginService
            UsersLoginService->>userLogin(DB) : Create userLogin
            UsersLoginService-->>AuthService : Send error & statusCode 400
            deactivate UsersLoginService
        end
        AuthService->>CommonService : Get expire accessToken
        activate CommonService
        CommonService-->>AuthService : Send response
        deactivate CommonService
        AuthService->>CommonService : Get expire refreshToken
        activate CommonService
        CommonService-->>AuthService : Send response
        deactivate CommonService
        AuthService->>CommonService : Get expire iat
        activate CommonService
        CommonService-->>AuthService : Send response
        deactivate CommonService
        AuthService-->>AuthService : Sign accessToken
        AuthService-->>AuthService : Sign refreshToken
        AuthService->>UsersRefreshtokenService : Update usersRefreshtoken
        alt error
            AuthService-->>AuthController : Send error & StatusCode
        else
            AuthService-->>AuthController : Send response & StatusCode 200
        end
        deactivate AuthService
    end
    AuthController-->>Client : Send response
    deactivate AuthController


```