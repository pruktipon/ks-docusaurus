```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthController
    participant AuthService

    Client->>UserGuard : POST : logout
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>AuthController : Send request
        deactivate UserGuard
        activate AuthController
        AuthController->>AuthService : Send request
        
        activate AuthService
        AuthService->>UsersLoginService : Create log logout
        activate UsersLoginService
        UsersLoginService-->>AuthService : Send Response
        deactivate UsersLoginService
        AuthService->>UsersRefreshtokenService : Clear refreshToken
        activate UsersRefreshtokenService
        UsersRefreshtokenService-->>AuthService : Send Response
        deactivate UsersRefreshtokenService
        alt error
            AuthService-->>AuthController :  Send error & statusCode
        else
            AuthService-->>AuthController :  Send response & statusCode 200
        end
        deactivate AuthService
    end
    AuthController-->>Client :  Send response
    deactivate AuthController
```