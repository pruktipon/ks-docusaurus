```mermaid
sequenceDiagram
    participant Client
    participant AddressController
    participant AddressService
    participant Database

    Client->>AddressController : GET : public/address/province/:provinceId/amphur/:amphurId/district
    activate AddressController
    AddressController->>AddressService : Send request
    activate AddressService
    AddressService->>Database : Query district by provinceId,amphurId
    activate Database
    Database-->>AddressService : Send response
    deactivate Database
    alt error
        AddressService-->>AddressController : Send error & StatusCode
    else
        AddressService-->>AddressController : Send response & StatusCode 200
    end
    deactivate AddressService
    AddressController-->>Client : Send response
    deactivate AddressController

```