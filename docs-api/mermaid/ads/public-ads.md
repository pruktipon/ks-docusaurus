```mermaid
sequenceDiagram
    participant Client
    participant AdsController
    participant AdsService
    participant ads(DB)

    Client->>AdsController : GET : public-ads
    activate AdsController
    AdsController->>AdsService : Send request
    activate AdsService
    AdsService->>ads(DB) : Query data
    activate ads(DB)
    ads(DB)-->>AdsService : Send response
    deactivate ads(DB)
    alt error
        AdsService-->>AdsController : Send error & StatusCode 500
    else
        AdsService-->>AdsController  : Send response & StatusCode 200
    end
    deactivate AdsService
    AdsController-->>Client :  Send response & StatusCode
    deactivate AdsController
```