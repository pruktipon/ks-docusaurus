```mermaid
sequenceDiagram
    participant Client
    participant WsJwtGuard
    participant AuthService
    participant WebSocket
    participant UserAttendService
    participant UsersService
    participant CourseService
    participant Database

    Client-->>WsJwtGuard : Connect gateway 
    activate WsJwtGuard
    WsJwtGuard-->>WsJwtGuard : Check Auth
    opt not send token
        WsJwtGuard-->>Client : Send error 
    end
    WsJwtGuard->>AuthService : Verify token
    activate AuthService
    deactivate WsJwtGuard
    opt token's expired
        AuthService-->>Client : Send statusCode 401
    end
    deactivate AuthService
    loop
        Client-->>WsJwtGuard : Message 'course-trial'
        activate WsJwtGuard
        WsJwtGuard-->>WsJwtGuard : Check Auth
        opt not send token
            WsJwtGuard-->>Client : Send error 
        end
        WsJwtGuard->>AuthService : Verify token
        activate AuthService
        deactivate WsJwtGuard
        opt token's expired
            AuthService-->>Client : Send statusCode 401
        end
        deactivate AuthService
        WsJwtGuard->>WebSocket : Send request
        activate WsJwtGuard
        activate WebSocket
        WebSocket->>UserAttendService : Send request
        activate UserAttendService
        deactivate WsJwtGuard
        
        UserAttendService->>UsersService : Find user by id
        activate UsersService
        UsersService-->>UserAttendService : Send response
        deactivate UsersService
        opt user isn't found
            UserAttendService-->>WebSocket : Send response error
        end
        UserAttendService->>CourseService : Find course by id
        activate CourseService
        CourseService-->>UserAttendService : Send response
        deactivate CourseService
        opt course isn't found
            UserAttendService-->>WebSocket : Send response error
        end
        UserAttendService->>Database : Query userAttendCourseTrial by user id,course id
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        opt userAttendCourseTrial isn't found
            UserAttendService-->>WebSocket : Send response error
        end
        opt course isn't expired
            UserAttendService-->>WebSocket : Send response error
        end
        UserAttendService->>Database : Query learnStamp last by userAttendCourseTrial id
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        UserAttendService->>Database : Create learnStamp 
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        UserAttendService->>Database : Update userAttendCourseTrial
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        UserAttendService-->>WebSocket : Send response
        deactivate UserAttendService
        WebSocket-->>Client : Send response by emit = 'msgToClient'
        deactivate WebSocket
    end
    Client-->>WsJwtGuard : Disconnect gateway
```