```mermaid
sequenceDiagram
    participant Client
    participant WsJwtGuard
    participant AuthService
    participant WebSocket
    participant UserAttendService
    participant UsersService
    participant CourseService
    participant ChapterService
    participant Database

    Client-->>WsJwtGuard : Connect gateway 
    activate WsJwtGuard
    WsJwtGuard-->>WsJwtGuard : Check Auth
    opt not send token
        WsJwtGuard-->>Client : Send error 
    end
    WsJwtGuard->>AuthService : Verify token
    activate AuthService
    deactivate WsJwtGuard
    opt token's expired
        AuthService-->>Client : Send statusCode 401
    end
    deactivate AuthService
    loop
        Client-->>WsJwtGuard : Message 'course'
        activate WsJwtGuard
        WsJwtGuard-->>WsJwtGuard : Check Auth
        opt not send token
            WsJwtGuard-->>Client : Send error 
        end
        WsJwtGuard->>AuthService : Verify token
        activate AuthService
        deactivate WsJwtGuard
        opt token's expired
            AuthService-->>Client : Send statusCode 401
        end
        deactivate AuthService
        WsJwtGuard->>WebSocket : Send request
        activate WsJwtGuard
        activate WebSocket
        WebSocket->>UserAttendService : Send request
        activate UserAttendService
        UserAttendService->>UsersService : Find user by id
        activate UsersService
        UsersService-->>UserAttendService : Send response
        deactivate UsersService
        opt user isn't found
            UserAttendService-->>WebSocket : Send response error
        end
        UserAttendService->>CourseService : Find course by id
        activate CourseService
        CourseService-->>UserAttendService : Send response
        deactivate CourseService
        opt course isn't found
            UserAttendService-->>WebSocket : Send response error
        end
        deactivate UserAttendService
        alt send chapterId
            UserAttendService->>ChapterService : Find chapter by id
            activate UserAttendService
            activate ChapterService
            ChapterService-->>UserAttendService : Send response
            deactivate ChapterService
            opt user isn't found
                UserAttendService-->>WebSocket : Send response error
            end
            UserAttendService->>Database : Query userAttendCourse by courseId & userId
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            opt userAttendCourse isn't found
                UserAttendService-->>WebSocket : Send response error
            end
            UserAttendService->>Database : Query userAttendChapter by courseId & userId
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            opt userAttendChapter isn't found
                UserAttendService-->>WebSocket : Send response error
            end
            UserAttendService->>Database : Query learnStamp by userAttendChapterId
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService->>Database : Create learnStamp 
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService->>Database : Update userAttendChapter
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService->>Database : Count userAttendChapter by userAttendCourseId
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService->>Database : Sum lastDuration userAttendChapter by userAttendCourseId
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService->>Database : Update userAttendCourse
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            deactivate UserAttendService
        else
            UserAttendService->>Database : Query userAttendCourse by userId & courseId
            activate UserAttendService
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            opt userAttendCourse isn't found
                UserAttendService-->>WebSocket : Send response error
            end
            UserAttendService->>Database : Query learnStamp by userAttendCourseId
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService->>Database : Create learnStamp 
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService->>Database : Update userAttendCourse 
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService-->>WebSocket : Send response
            deactivate UserAttendService
        end
        WebSocket-->>Client : Send response by emit = 'msgToClient'
        deactivate WebSocket
    end
    Client-->>WsJwtGuard : Disconnect gateway
```