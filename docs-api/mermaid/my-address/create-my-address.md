```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UserAddressController
    participant UserAddressService
    participant AddressService
    participant UsersService
    participant Database

    Client->>UserGuard : POST : my-address
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UserAddressController : Send request
        activate UserAddressController
    end
    deactivate UserGuard
    UserAddressController->>UserAddressService : Service create
    activate UserAddressService
    UserAddressService->>AddressService : Service provinceById
    activate AddressService
    AddressService-->>UserAddressService : Send response
    deactivate AddressService
    UserAddressService->>AddressService : Service amphurById
    activate AddressService
    AddressService-->>UserAddressService : Send response
    deactivate AddressService
    UserAddressService->>AddressService : Service districtById
    activate AddressService
    AddressService-->>UserAddressService : Send response
    deactivate AddressService
    UserAddressService->>UsersService : Service findOnebyId
    activate UsersService
    UsersService-->>UserAddressService : Send response
    deactivate UsersService
    opt set default
        UserAddressService-->>UserAddressService : Service setAllAddressNotDefaut
    end
    UserAddressService->>Database : Create userAddress
    activate Database
    Database-->>UserAddressService : Send response
    deactivate Database
    UserAddressService->>Database : Find userAddress by addressId
    activate Database
    Database-->>UserAddressService : Send response
    deactivate Database
    alt error
        UserAddressService-->>UserAddressController : Send error message & StatusCode
    else
        UserAddressService-->>UserAddressController : Send response & StatusCode 201
    end
    deactivate UserAddressService
    UserAddressController-->>Client : Send response
    deactivate UserAddressController
```
