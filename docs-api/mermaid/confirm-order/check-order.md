```mermaid
sequenceDiagram
    participant Client
    participant OrderController
    participant UserCourseCartService
    participant Database

    Client->>OrderController : GET : confirm-order/check
    activate OrderController
    OrderController->>UserCourseCartService : Service findOneByNo
    activate UserCourseCartService
    UserCourseCartService->>Database : Query userCourseCart by no
    activate Database
    Database-->>UserCourseCartService : Send response
    deactivate Database 
    opt userCourseCart isn't found
        UserCourseCartService-->>OrderController : Send error Message & statusCode 404
    end
    UserCourseCartService-->>OrderController : Send response
    deactivate UserCourseCartService
    OrderController-->>Client : Send response
    deactivate OrderController
```