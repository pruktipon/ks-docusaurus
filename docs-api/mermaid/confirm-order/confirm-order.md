```mermaid
sequenceDiagram
    participant Client
    participant OrderController
    participant UserCourseCartService
    participant StatusCartService
    participant UserAttendService
    participant CourseService
    participant StatusAttendService
    participant ChapterService
    participant Database

    Client->>OrderController : GET : confirm-order
    activate OrderController
    OrderController->>UserCourseCartService : Service confirmOrder
    activate UserCourseCartService
    UserCourseCartService->>UserCourseCartService : Service findOneByNo
    UserCourseCartService->>Database : Query userCourseCart by no
    activate Database
    Database-->>UserCourseCartService : Send response
    deactivate Database 
    opt userCourseCart isn't found
        UserCourseCartService-->>OrderController : Send error Message & statusCode 404
    end
    UserCourseCartService->>StatusCartService : Service findOnebyId
    activate StatusCartService
    StatusCartService->>Database : Query statusCart by statusId
    activate Database
    Database-->>StatusCartService : Send response
    deactivate Database
    StatusCartService-->>UserCourseCartService : Send response
    deactivate StatusCartService
    UserCourseCartService->>Database : Update userCourseCart 
    activate Database
    Database-->>UserCourseCartService : Send response
    deactivate Database
    UserCourseCartService->>UserAttendService : Service registerCourse
    activate UserAttendService
    UserAttendService->>UserCourseCartService : Service findOneByNo
    UserCourseCartService-->>UserAttendService  : Send response

    UserAttendService->>CourseService : Service findOneByIdAndRelation
    activate CourseService
    CourseService-->>UserAttendService  : Send response
    deactivate CourseService

    UserAttendService->>StatusAttendService : Service findById
    activate StatusAttendService
    StatusAttendService-->>UserAttendService  : Send response
    deactivate StatusAttendService

    UserAttendService->>ChapterService : Service findOneByCourseId
    activate ChapterService
    ChapterService-->>UserAttendService  : Send response
    deactivate ChapterService

    UserAttendService->>Database : Query userAttendCourse by userId,courseId
    activate Database
    Database-->>UserAttendService  : Send response
    deactivate Database

    opt userAttendCourse's found
        UserAttendService-->>UserCourseCartService : Send error Message & statusCode 409
    end

    UserAttendService->>Database : Update userAttendCourse by userAttendCourseId
    activate Database
    Database-->>UserAttendService  : Send response
    deactivate Database

    loop chapters
        UserAttendService->>Database : Create userAttendChapter
        activate Database
        Database-->>UserAttendService  : Send response
        deactivate Database
    end 
    UserAttendService-->>UserCourseCartService : Send response
    deactivate UserAttendService
    UserCourseCartService-->>OrderController : Send response
    deactivate UserCourseCartService
    OrderController-->>Client : Send response
    deactivate OrderController
```