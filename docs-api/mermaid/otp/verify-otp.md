```mermaid
sequenceDiagram
    participant Client
    participant OtpController
    participant OtpService
    participant Database

    Client->>OtpController : POST : otp/verify
    activate OtpController
    OtpController->>OtpService : Service verify
    activate OtpService
    OtpService->>Database : Query otp by referenseCode
    activate Database
    Database-->>OtpService : Send response
    deactivate Database
    alt otp isn't found
        OtpService->>OtpService : Error statusCode 404
    else otp's found
        opt otp's expired
            OtpService->>OtpService : Error statusCode 4001
        end
        opt code's wrong
            OtpService->>OtpService : Error statusCode 4002
        end
        OtpService->>Database : Update otp
        activate Database
        Database-->>OtpService : Send response
        deactivate Database
    end
    alt error
        OtpService-->>OtpController : Send error message & StatusCode
    else not error
        OtpService-->>OtpController : Send response & StatusCode 200
    end
    deactivate OtpService
    OtpController-->>Client : Send response
    deactivate OtpController

```
