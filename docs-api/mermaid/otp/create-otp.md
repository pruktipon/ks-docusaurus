```mermaid
sequenceDiagram
    participant Client
    participant OtpController
    participant OtpService
    participant Database

    Client->>OtpController : POST : otp
    activate OtpController
    OtpController->>OtpService : Service create
    activate OtpService
    OtpService->>OtpService:Service checkSendAgian
    OtpService->>Database : Query otp by phoneNumber
    activate Database
    Database-->>OtpService : Send response
    deactivate Database
    alt can't send otp
        OtpService-->>OtpController : Send response & StatusCode 200
    else can send otp
        OtpService->>OtpService: Service generateOTP
        OtpService->>OtpService : ramdom code
        OtpService-->>OtpService : Send response
        OtpService->>Database : Create otp
        activate Database
        Database-->>OtpService : Send response
        deactivate Database
        alt error
            OtpService-->>OtpController : Send error message & StatusCode
        else not error
            OtpService-->>OtpController : Send response & StatusCode 200
        end
    end
    deactivate OtpService
    OtpController-->>Client : Send response
    deactivate OtpController

```
