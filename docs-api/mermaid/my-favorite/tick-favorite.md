```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UserFavoriteCourseController
    participant UserFavoriteCourseService
    participant UsersService
    participant CourseService
    participant Database

    Client->>UserGuard : PATCH : my-favorite/:courseId
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UserFavoriteCourseController : Send request
        activate UserFavoriteCourseController
    end
    deactivate UserGuard
    
    UserFavoriteCourseController->>UserFavoriteCourseService : Service tickFavorite
    activate UserFavoriteCourseService
    UserFavoriteCourseService->>Database:Query userFavoriteCourse by userId,courseId
    activate Database
    Database-->>UserFavoriteCourseService: Send response
    deactivate Database
    
    alt userFavoriteCourse's found
        UserFavoriteCourseService->>UserFavoriteCourseService:Service removeFavorite
        UserFavoriteCourseService->>Database:Query userFavoriteCourse by userId,courseId
        activate Database
        Database-->>UserFavoriteCourseService: Send response
        deactivate Database
        UserFavoriteCourseService->>CourseService : Service updateAmountFavorite
        activate CourseService
        CourseService-->>UserFavoriteCourseService : Send response
        deactivate CourseService
    else userFavoriteCourse isn't found
        UserFavoriteCourseService->>UserFavoriteCourseService:Service createFavorite
        UserFavoriteCourseService->>UsersService : Service findOnebyId
        activate UsersService
        UsersService-->>UserFavoriteCourseService : Send response
        deactivate UsersService
        UserFavoriteCourseService->>CourseService : Service findOneById
        activate CourseService
        CourseService-->>UserFavoriteCourseService : Send response
        deactivate CourseService
        UserFavoriteCourseService->>Database:Create userFavoriteCourse
        activate Database
        Database-->>UserFavoriteCourseService: Send response
        deactivate Database
        UserFavoriteCourseService->>CourseService : Service updateAmountFavorite
        activate CourseService
        CourseService-->>UserFavoriteCourseService : Send response
        deactivate CourseService
    end
    alt error
        UserFavoriteCourseService-->>UserFavoriteCourseController : Send error message & StatusCode
    else not error
        UserFavoriteCourseService-->>UserFavoriteCourseController : Send response & StatusCode 200
    end
    deactivate UserFavoriteCourseService
    UserFavoriteCourseController-->>Client : Send response
    deactivate UserFavoriteCourseController
```