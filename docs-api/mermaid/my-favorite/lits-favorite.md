```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UserFavoriteCourseController
    participant UserFavoriteCourseService
    participant UserAttendService
    participant Database

    Client->>UserGuard : GET : my-favorite
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UserFavoriteCourseController : Send request
        activate UserFavoriteCourseController
    end
    deactivate UserGuard
    UserFavoriteCourseController->>UserFavoriteCourseService : Service myFavorite
    activate UserFavoriteCourseService
    UserFavoriteCourseService->>Database:Query userFavoriteCourse by userId
    activate Database
    Database-->>UserFavoriteCourseService: Send response
    deactivate Database
    loop userFavoriteCourse
        UserFavoriteCourseService-->>UserFavoriteCourseService: Map userFavoriteCourse & response
    end
    UserFavoriteCourseService->>UserAttendService : Service myCourse
    activate UserAttendService
    UserAttendService-->>UserFavoriteCourseService : Send response
    deactivate UserAttendService
    UserFavoriteCourseService-->>UserFavoriteCourseService : Map data response userFavoriteCourse & data from UserAttendService 
    alt error
        UserFavoriteCourseService-->>UserFavoriteCourseController : Send error message & StatusCode
    else
        UserFavoriteCourseService-->>UserFavoriteCourseController : Send response & StatusCode 200
    end
    deactivate UserFavoriteCourseService
    UserFavoriteCourseController-->>Client : Send response
    deactivate UserFavoriteCourseController
```