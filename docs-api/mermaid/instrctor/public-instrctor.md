```mermaid
sequenceDiagram
    participant Client
    participant InstrctorController
    participant InstrctorService
    participant Database

    Client->>InstrctorController : GET : public/instrctor
    activate InstrctorController
    InstrctorController->>InstrctorService : Send request
    activate InstrctorService
    InstrctorService->>Database : Query category
    activate Database
    Database-->>InstrctorService : Send response
    deactivate Database
    alt error
        InstrctorService-->>InstrctorController : Send error & StatusCode
    else
        InstrctorService-->>InstrctorController : Send response & StatusCode
    end
    deactivate InstrctorService
    InstrctorController-->>Client : Send response
    deactivate InstrctorController

```