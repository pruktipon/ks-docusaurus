```mermaid
sequenceDiagram
    participant Client

    participant UserAttendController
    participant UserAttendService
    participant Database

    Client->>UserAttendController : GET : my-course/status
    activate UserAttendController
    UserAttendController->>UserAttendService : Service statusMyCourse
    activate UserAttendService
    UserAttendService->>Database:Query statusAttend
    activate Database
    Database-->>UserAttendService : Send response
    deactivate Database
    alt error
        UserAttendService-->>UserAttendController : Send error message & StatusCode
    else
        UserAttendService-->>UserAttendController : Send response & StatusCode 200
    end
    deactivate UserAttendService
    UserAttendController-->>Client : Send response
    deactivate UserAttendController


```