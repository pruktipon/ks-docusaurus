```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UserAttendController
    participant UserAttendService
    participant CourseService
    participant Database

    Client->>UserGuard : GET : my-course/course/:courseId
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UserAttendController : Send request
        activate UserAttendController
    end
    deactivate UserGuard
    UserAttendController->>UserAttendService : Service dataLearn
    activate UserAttendService
    alt course's trial
        UserAttendService->>CourseService : Service findOneByIdAndRelation
        activate CourseService
        CourseService-->>UserAttendService : Send response
        deactivate CourseService
        opt startDate's empty
            UserAttendService->>Database : Update userAttendCourseTrial
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
        end
        loop
            UserAttendService-->>UserAttendService : Map course file & response
        end
    else
        UserAttendService->>Database : Query userAttendCourse
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        loop
            UserAttendService-->>UserAttendService : Map chapter & response
        end
    end
    alt error
        UserAttendService-->>UserAttendController : Send error message & StatusCode
    else
        UserAttendService-->>UserAttendController : Send response & StatusCode 200
    end
    deactivate UserAttendService
    UserAttendController-->>Client : Send response
    deactivate UserAttendController


```