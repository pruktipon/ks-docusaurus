```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UserAttendController
    participant UserAttendService
    participant Database

    Client->>UserGuard : GET : my-course
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UserAttendController : Send request
        activate UserAttendController
    end
    deactivate UserGuard
    UserAttendController->>UserAttendService : Service myCourse
    activate UserAttendService
    UserAttendService->>Database : Query userAttendCourse by userId
    activate Database
    Database-->>UserAttendService : Send response
    deactivate Database
    loop userAttendCourse
        UserAttendService-->>UserAttendService : Map data & response
    end
     alt error
        UserAttendService-->>UserAttendController : Send error message & StatusCode
    else
        UserAttendService-->>UserAttendController : Send response & StatusCode 200
    end
    deactivate UserAttendService
    UserAttendController-->>Client : Send response
    deactivate UserAttendController


```