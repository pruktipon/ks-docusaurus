```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UserCourseCartController
    participant UserCourseCartService
    participant StatusCartService
    participant Database

    Client->>UserGuard : PATCH : my-cart/:cartId/status
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UserCourseCartController : Send request
        activate UserCourseCartController
    end
    deactivate UserGuard
    UserCourseCartController->>UserCourseCartService : Service updateStatusOnCart
    activate UserCourseCartService
    UserCourseCartService->>Database : Query userCourseCart by userId,cartId
    activate Database
    Database-->>UserCourseCartService : Send response
    deactivate Database
    opt userCourseCart isn't found
        UserCourseCartService-->>UserCourseCartService : Send error message & StatusCode 404
    end
     opt status's = success
          UserCourseCartService-->>UserCourseCartService : Send error message & StatusCode 409
    end
    UserCourseCartService->>StatusCartService : Service findOnebyId
    StatusCartService-->>UserCourseCartService : Send response
    opt data from StatusCartService isn't found
        UserCourseCartService-->>UserCourseCartService : Send error message & StatusCode 404
    end
    
    UserCourseCartService->>Database : Update userCourseCart by id
    activate Database
    Database-->>UserCourseCartService : Send response
    deactivate Database
    alt error
        UserCourseCartService-->>UserCourseCartController : Send error message & StatusCode
    else
        UserCourseCartService-->>UserCourseCartController : Send response & StatusCode 20
    end
    deactivate UserCourseCartService
    UserCourseCartController-->>Client : Send response
    deactivate UserCourseCartController
```