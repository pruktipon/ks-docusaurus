```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UserCourseCartController
    participant UserCourseCartService
    participant Database

    Client->>UserGuard : GET : my-cart
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UserCourseCartController : Send request
        activate UserCourseCartController
    end
    deactivate UserGuard
    UserCourseCartController->>UserCourseCartService : Service myCart
    activate UserCourseCartService
    UserCourseCartService->>Database : Query userCourseCart by userId
    activate Database
    Database-->>UserCourseCartService : Send response
    deactivate Database
    alt error
        UserCourseCartService-->>UserCourseCartController : Send error message & StatusCode
    else
        UserCourseCartService-->>UserCourseCartController : Send response & StatusCode 200
    end
    deactivate UserCourseCartService
    UserCourseCartController-->>Client : Send response
    deactivate UserCourseCartController
```