```mermaid
sequenceDiagram
    participant Client
    participant CategoryController
    participant CategoryService
    participant category(DB)
    participant subcategory(DB)

    Client->>CategoryController : GET : public/category/:categoryId/subCategories
    activate CategoryController
    CategoryController->>CategoryService : Send request
    activate CategoryService
    CategoryService-->>category(DB) : Find category by id
    activate category(DB)
    category(DB)-->>CategoryService : Send response
    deactivate category(DB)
    alt category isn't found
        CategoryService-->>CategoryController : Send statusCode 404
    else
        CategoryService->>subcategory(DB) : Query subcategory by category id
        activate subcategory(DB)
        subcategory(DB)-->>CategoryService : Send response
        deactivate subcategory(DB)
    end
    CategoryService-->>CategoryController : Send response & StatusCode
    deactivate CategoryService
    CategoryController-->>Client : Send response
    deactivate CategoryController

```