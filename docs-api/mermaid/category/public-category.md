```mermaid
sequenceDiagram
    participant Client
    participant CategoryController
    participant CategoryService
    participant category(DB)

    Client->>CategoryController : GET : public/category
    activate CategoryController
    CategoryController->>CategoryService : Send request
    activate CategoryService
    CategoryService->>category(DB) : Query category
    activate category(DB)
    category(DB)-->>CategoryService : Send response
    deactivate category(DB)
    alt error
        CategoryService-->>CategoryController : Send error & StatusCode
    else
        CategoryService-->>CategoryController : Send response & StatusCode
    end
    deactivate CategoryService
    CategoryController-->>Client : Send response
    deactivate CategoryController

```