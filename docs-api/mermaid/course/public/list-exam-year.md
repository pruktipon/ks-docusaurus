```mermaid
sequenceDiagram
    participant Client
    participant CourseController
    participant CourseService
    participant Database

    Client->>CourseController : GET : public/course/exam-year
    activate CourseController
    CourseController->>CourseService : Send request
    activate CourseService
    CourseService->>Database : Query exam year group by course
    activate Database
    Database-->>CourseService : Send response
    deactivate Database
    alt error
        CourseService-->>CourseController : Send error & StatusCode 500
    else
        CourseService-->>CourseController : Send response & StatusCode 200
    end
    deactivate CourseService
    CourseController-->>Client : Send response
    deactivate CourseController
```