```mermaid
sequenceDiagram
    participant Client
    participant CourseController
    participant CourseService
    participant Database
    Client->>CourseController : GET : public/course/:courseId
    activate CourseController
    CourseController->>CourseService : Send request
    activate CourseService
    alt course's trial
        CourseService->>Database : Query course by course id and course's trial
        activate Database
        Database-->>CourseService : Send response
        deactivate Database
        alt course isn't found
            CourseService-->>CourseController : Send statusCode 404
        else
            CourseService-->>CourseController : Send response & statusCode 200
        end
    else
        CourseService->>Database : Query course by course id
        activate Database
        Database-->>CourseService : Send response
        deactivate Database
        alt course isn't found
            CourseService-->>CourseController : Send statusCode 404
        else
            CourseService-->>CourseController : Send response & statusCode 200
        end
    end
    deactivate CourseService
    CourseController-->>Client : Send response
    deactivate CourseController
```