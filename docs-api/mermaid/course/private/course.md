```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant CourseController
    participant CourseService
    participant UserAttendService
    participant Database
    Client->>UserGuard : GET : course
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>CourseController : Send request
        activate CourseController
    end
    deactivate UserGuard
    CourseController->>CourseService : Send request
    
    activate CourseService
    alt course's trial
        CourseService->>Database : Query course by course id and course's trial
        activate Database
        Database-->>CourseService : Send response
        deactivate Database
        alt course isn't found
            CourseService-->>CourseController : Send statusCode 404
        else
            CourseService->>UserAttendService : all student trial
            activate UserAttendService
            UserAttendService->>Database : Count student on course trial
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService-->>CourseService : Send response
            deactivate UserAttendService
            
            CourseService-->>CourseController : Send response & statusCode 200
        end
    else
        CourseService->>Database : Query course by course id
        activate Database
        Database-->>CourseService : Send response
        deactivate Database
        alt course isn't found
            CourseService-->>CourseController : Send statusCode 404
        else
            CourseService->>UserAttendService : my course 
            activate UserAttendService
            UserAttendService->>Database : Query your course 
            activate Database
            Database-->>UserAttendService : Send response
            deactivate Database
            UserAttendService-->>CourseService : Send response
            deactivate UserAttendService
            loop data your course 
                CourseService-->>CourseService : map data course & your course
            end
            CourseService-->>CourseController : Send response & statusCode 200
        end
    end
    deactivate CourseService
    CourseController-->>Client : Send response
    deactivate CourseController
```