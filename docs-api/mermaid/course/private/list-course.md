```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant CourseController
    participant CourseService
    participant UserAttendService
    participant Database

    Client->>UserGuard : GET : course
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>CourseController : Send request
        activate CourseController
    end
    deactivate UserGuard
    CourseController->>CourseService : Send request
    activate CourseService
    CourseService->>Database : Query course 's isPublish
    activate Database
    Database-->>CourseService : Send response
    deactivate Database
    alt course's trial
        CourseService->>UserAttendService : my course trial
        activate UserAttendService
        UserAttendService->>Database : Query your course trial
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        UserAttendService-->>CourseService : Send response
        deactivate UserAttendService
        loop data your course trial
            CourseService-->>CourseService : map data course & your course
        end
        CourseService-->>CourseController : Send response & statusCode 200
    else
        CourseService->>UserAttendService : my course 
        activate UserAttendService
        UserAttendService->>Database : Query your course 
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        UserAttendService-->>CourseService : Send response
        deactivate UserAttendService
        loop data your course 
            CourseService-->>CourseService : map data course & your course
        end
        CourseService->>UserAttendService : my favorite course 
        activate UserAttendService
        UserAttendService->>Database : Query your favorite course 
        activate Database
        Database-->>UserAttendService : Send response
        deactivate Database
        UserAttendService-->>CourseService : Send response
        deactivate UserAttendService
        loop data your course 
            CourseService-->>CourseService : map data course & your favorite course
        end
    end
    alt error
        CourseService-->>CourseController : Send error & StatusCode 500
    else
        CourseService-->>CourseController : Send response & StatusCode 200
    end
    deactivate CourseService
    CourseController-->>Client : Send response
    deactivate CourseController
```