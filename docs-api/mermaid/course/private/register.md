```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant CourseController
    participant CourseService
    participant UserAttendService
    participant RunningNoService
    participant UserCourseCartService
    participant StatusCartService
    participant UserAddressService
    participant UserService
    participant Database
    Client->>UserGuard : POST : course/:courseId/register
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>CourseController : Send request
        activate CourseController
    end
    deactivate UserGuard
    CourseController->>CourseService : Send request
    activate CourseService
    alt course's trial
        CourseService->>Database : Create userAttendCourseTrial
        activate Database
        Database-->>CourseService : Send Response
        deactivate Database
        CourseService->>Database : Update amount learner
        activate Database
        Database-->>CourseService : Send Response 
        deactivate Database
        CourseService-->>CourseController : Send response 
        
    else
        CourseService->>RunningNoService : Send request
        activate RunningNoService
        RunningNoService->>Database : Create running no
        activate Database
        Database-->>RunningNoService : Send response
        deactivate Database
        RunningNoService-->>CourseService : Send response
        deactivate RunningNoService
        CourseService->>UserCourseCartService : Send request
        activate UserCourseCartService
        UserCourseCartService->>CourseService : Find course by id
        deactivate UserCourseCartService
        CourseService->>Database : Query course by id
        activate Database
        Database-->>CourseService : Send Response
        deactivate Database
        CourseService-->>UserCourseCartService : Send Response
        activate UserCourseCartService
        opt course isn't found
            UserCourseCartService-->>CourseController : Send response & StatusCode 404
        end
        UserCourseCartService->>UserService : Find user by id
        activate UserService
        UserService->>Database : Query user by id
        activate Database
        Database-->>UserService : Send Response
        deactivate Database
        UserService-->>UserCourseCartService : Send Response
        deactivate UserService
        opt user isn't found
            UserCourseCartService-->>CourseController : Send response & StatusCode 404
        end
        UserCourseCartService->>StatusCartService : Find status cart by id
        activate StatusCartService
        StatusCartService->>Database : Query statusCart by id
        activate Database
        Database-->>StatusCartService : Send Response
        deactivate Database
        StatusCartService-->>UserCourseCartService : Send Response
        deactivate StatusCartService
        UserCourseCartService->>UserAddressService : Find address by id
        activate UserAddressService
        UserAddressService->>Database : Query address by id
        activate Database
        Database-->>UserAddressService : Send Response
        deactivate Database
        UserAddressService-->>UserCourseCartService : Send Response
        deactivate UserAddressService
        UserCourseCartService->>Database : Add course to cart
        activate Database
        Database-->>UserCourseCartService : Send response
        deactivate Database
        UserCourseCartService-->>CourseService : Send response
        deactivate UserCourseCartService
        CourseService-->>CourseController : Send response 
        deactivate CourseService
    end
    CourseController-->>Client : Send response & StatusCode
    deactivate CourseController
```