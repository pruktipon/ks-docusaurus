```mermaid
sequenceDiagram
    participant Client
    participant UsersController
    participant UsersService
    participant CommonService
    participant AuthService
    participant UsersResetpasswordService
    participant Database

    Client->>UsersController : POST : users/reset-password
    activate UsersController
    UsersController->>UsersService : Service 'resetPassword'
    activate UsersService
    UsersService->>UsersService : Service 'findOnebyEmail'
    UsersService->>Database : Query user by email
    activate Database
    Database-->>UsersService : Send response
    deactivate Database
    UsersService-->>UsersService : Send response
    alt user isn't found
        UsersService-->>UsersService : Error statusCode 404
    else
        UsersService->>CommonService : Service 'getExpireTime' 
        activate CommonService
        CommonService-->>UsersService : Send response
        deactivate CommonService
        UsersService->>CommonService : Service 'getExpireTime' 
        activate CommonService
        CommonService-->>UsersService : Send response
        deactivate CommonService
        UsersService->>AuthService : Service 'signToken'
        activate AuthService
        AuthService-->>UsersService : Send response
        deactivate AuthService
        UsersService->>UsersResetpasswordService : Service 'create'
        activate UsersResetpasswordService
        UsersResetpasswordService-->>UsersService : Send response
        deactivate UsersResetpasswordService
        UsersService-->>UsersService : Service sendEmail
        UsersService->>CommonService : Service 'sendEmailWithTemplate'
        activate CommonService
        CommonService-->>UsersService : Send response
        deactivate CommonService
    end
    alt error
        UsersService-->>UsersController : Send error message & StatusCode
    else
        UsersService-->>UsersController : Send response & StatusCode 200
    end
    deactivate UsersService
    UsersController-->>Client : Send response
    deactivate UsersController
```