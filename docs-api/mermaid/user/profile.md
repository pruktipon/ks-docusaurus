```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UsersController
    participant UsersService
    participant Database

    Client->>UserGuard : GET : users/profile
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UsersController : Send request
        activate UsersController
    end
    deactivate UserGuard
    UsersController->>UsersService : Service findOnebyId
    activate UsersService
    UsersService->>Database:Query users by userId
    activate Database
    Database-->>UsersService:Send response
    deactivate Database
    opt user's found
        UsersService-->>UsersService : Error statusCode 404
    end
    alt error
        UsersService-->>UsersController : Send error message & StatusCode
    else
        UsersService-->>UsersController : Send response & StatusCode 200
    end
    deactivate UsersService
    UsersController-->>Client : Send response
    deactivate UsersController
```