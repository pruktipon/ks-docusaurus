```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UsersController
    participant UsersService
    participant UsersResetpasswordService
    participant Database

    Client->>UserGuard : POST : users/new-password
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UsersController : Send request
        activate UsersController
    end
    deactivate UserGuard
    UsersController->>UsersService : Service 'newPassword'
    activate UsersService
    UsersService->>UsersResetpasswordService:Service 'findOne'
    activate UsersResetpasswordService
    UsersResetpasswordService-->>UsersService: Send response
    deactivate UsersResetpasswordService
    alt token isn't found
        UsersService-->>UsersService : Error statusCode 404
    else
        UsersService->>AuthService:Service 'verifyToken'
        activate AuthService
        AuthService-->>UsersService : Send response
        deactivate AuthService
        UsersService->>Database : Query user by userId
        activate Database
        Database-->>UsersService : Send response
        deactivate Database
        alt user isn't found
            UsersService-->>UsersService : Error statusCode 404
        else
            UsersService->>UsersService: Service 'updatePassword'
            UsersService->>UsersService: Service 'findOne'
            UsersService->>Database : Query users by query json
            activate Database
            Database-->>UsersService : Send response
            deactivate Database
            UsersService-->>UsersService : Encode password
            UsersService->>UsersResetpasswordService : Service 'create'
            activate UsersResetpasswordService
            UsersResetpasswordService-->>UsersService : Send response
            deactivate UsersResetpasswordService
            UsersService->>UsersService: Service 'findOnebyId'
            UsersService->>Database : Query users by userId
            activate Database
            Database-->>UsersService : Send response
            deactivate Database
            UsersService-->>UsersService:Send response
            UsersService->>Database : Update user
            activate Database
            Database-->>UsersService : Send response
            deactivate Database
            UsersService-->>UsersService:Send response
            UsersService-->>UsersService:Send response
        end
    end
    alt error
        UsersService-->>UsersController : Send error message & StatusCode
    else
        UsersService-->>UsersController : Send response & StatusCode 200
    end
    deactivate UsersService
    UsersController-->>Client : Send response
    deactivate UsersController
```