```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UsersController
    participant UsersService
    participant RoleService
    participant Database

    Client->>UserGuard : POST : users
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UsersController : Send request
        activate UsersController
    end
    deactivate UserGuard
    UsersController->>UsersService : Service create
    activate UsersService
    UsersService->>UsersService : Service checkEmail
    UsersService->>Database : Query users by email
    activate Database
    Database-->>UsersService : Send response
    deactivate Database
    alt email's ready
        UsersService-->>UsersService : Error statusCode 409
    else
        UsersService-->>UsersService : Send response
        UsersService-->>UsersService : Encode password
        UsersService->>RoleService : Service findOneById
        activate RoleService
        RoleService-->>UsersService: Send response
        deactivate RoleService
        UsersService->>Database : Create users 
        activate Database
        Database-->>UsersService : Send response
        deactivate Database
    end
    alt error
        UsersService-->>UsersController : Send error message & StatusCode
    else
        UsersService-->>UsersController : Send response & StatusCode 201
    end
    deactivate UsersService
    UsersController-->>Client : Send response
    deactivate UsersController
```