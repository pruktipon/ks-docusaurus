```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UsersController
    participant UsersService
    participant UsersResetpasswordService
    participant Database

    Client->>UserGuard : PATCH : users/password
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UsersController : Send request
        activate UsersController
    end
    deactivate UserGuard
    UsersController->>UsersService: Service 'updatePassword'
    activate UsersService
            UsersService->>UsersService: Service 'findOne'
            UsersService->>Database : Query users by query json
            activate Database
            Database-->>UsersService : Send response
            deactivate Database
            UsersService-->>UsersService : Encode password
            UsersService->>UsersResetpasswordService : Service 'create'
            activate UsersResetpasswordService
            UsersResetpasswordService-->>UsersService : Send response
            deactivate UsersResetpasswordService
            UsersService->>UsersService: Service 'findOnebyId'
            UsersService->>Database : Query users by userId
            activate Database
            Database-->>UsersService : Send response
            deactivate Database
            UsersService-->>UsersService:Send response
            UsersService->>Database : Update user
            activate Database
            Database-->>UsersService : Send response
            deactivate Database
            UsersService-->>UsersService:Send response
            UsersService-->>UsersService:Send response
    
    alt error
        UsersService-->>UsersController : Send error message & StatusCode
    else
        UsersService-->>UsersController : Send response & StatusCode 200
    end
    deactivate UsersService
    UsersController-->>Client : Send response
    deactivate UsersController
```