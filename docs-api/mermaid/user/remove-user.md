```mermaid
sequenceDiagram
    participant Client
    participant UserGuard
    participant AuthService
    participant UsersController
    participant UsersService
    participant UsersLoginService
    participant Database

    Client->>UserGuard : Delete : users
    activate UserGuard
    alt accessToken not send
        UserGuard-->>UserGuard : Send error & statusCode 401
    else
        UserGuard->>AuthService : Verify token
        activate AuthService
        AuthService->>AuthService : Send response
        alt token's expired
            AuthService->>UserGuard : Send error & statusCode 401
        else
            AuthService->>UserGuard : Send response 
        end
        deactivate AuthService
        UserGuard->>UsersController : Send request
        activate UsersController
    end
    deactivate UserGuard
    UsersController->>UsersService : Service 'delete'
    activate UsersService
    UsersService->>UsersService: Service 'findOnebyId'
    UsersService->>Database : Query users by userId
    activate Database
    Database-->>UsersService : Send response
    deactivate Database
    UsersService-->>UsersService :Send response
    UsersService->>UsersLoginService: Service 'create'
    UsersLoginService-->>UsersService : Send response
    UsersService->>Database : remove user
    activate Database
    Database-->>UsersService : Send response
    deactivate Database
    alt error
        UsersService-->>UsersController : Send error message & StatusCode
    else
        UsersService-->>UsersController : Send response & StatusCode 200
    end
    deactivate UsersService
    UsersController-->>Client : Send response
    deactivate UsersController
```